
[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |   

|[ก่อนหน้า](ch1-08-01TH.md)| [ต่อไป](ch1-08-03TH.md) |[ต่อไป บทที่ 2](ch2TH.md) |
| ---------- | ---------- | ---------- |    

### 1.8.2 [การเขียนเวกเตอร์หนึ่งหน่วยในพิกัดแบบทรงกลมในรูปของเวกเตอร์หนึ่งหน่วยในพิกัดฉาก](ch1-08-02TH.md)    
ในหัวข้อนี้เราต้องการเขียน  
```math
\mathbf a_r = k_{r,x} \mathbf a_x + k_{r,y} \mathbf a_y + k_{r,z} \mathbf a_z \newline  
\mathbf a_\theta = k_{\theta,x} \mathbf a_x + k_{\theta,y} \mathbf a_y + k_{\theta,z} \mathbf a_z \newline  
\mathbf a_\phi = k_{\phi,x} \mathbf a_x + k_{\phi,y} \mathbf a_y + k_{\phi,z} \mathbf a_z \newline  

```  
ซึ่ง $`k_{i,j}`$ คือส่วนของเวกเตอร์หนึ่งหน่วยที่ชี้ไปในทิศทาง $`i`$ ที่ชี้ไปในทิศทาง $`j`$ จากที่เคยอธิบายแล้วในหัวข้อ[การหาส่วนของเวกเตอร์ในทิศทางใดทิศทางหนึ่ง](https://gitlab.com/gitlabdcw/lecture-notes-on-em/-/blob/master/contents/ch1TH/ch1-08TH.md#การหาส่วนของเวกเตอร์ในทิศทางใดทิศทางหนึ่ง)  
```math
k_{r,x} =\mathbf a_r \cdot \mathbf a_x = |\mathbf a_r| |\mathbf a_x| \cos \beta_{r,x} \newline 
k_{r,y} =\mathbf a_r \cdot \mathbf a_y  = |\mathbf a_r| |\mathbf a_y| \cos \beta_{r,y} \newline 
k_{r,z} =\mathbf a_r \cdot \mathbf a_z  = |\mathbf a_r| |\mathbf a_z| \cos \beta_{r,z}\newline 
k_{\theta,x} =\mathbf a_\theta \cdot \mathbf a_x  = |\mathbf a_\theta| |\mathbf a_x| \cos \beta_{\theta,x} \newline 
k_{\theta,y} =\mathbf a_\theta \cdot \mathbf a_y = |\mathbf a_\theta| |\mathbf a_y| \cos \beta_{\theta,y} \newline  
k_{\theta,z} =\mathbf a_\theta \cdot \mathbf a_z = |\mathbf a_\theta| |\mathbf a_z| \cos \beta_{\theta,z} \newline  
k_{\phi,x} =\mathbf a_\phi \cdot \mathbf a_x = |\mathbf a_\phi| |\mathbf a_x| \cos \beta_{\phi,x} \newline 
k_{\phi,y} =\mathbf a_\phi \cdot \mathbf a_y = |\mathbf a_\phi| |\mathbf a_y| \cos \beta_{\phi,y} \newline 
k_{\phi,z} =\mathbf a_\phi \cdot \mathbf a_z = |\mathbf a_\phi| |\mathbf a_z| \cos \beta_{\phi,z} \newline 

```  
จากรูป $`(a)`$ เราทราบแค่มุมระหว่าง $`\mathbf a_\phi`$ กับ เวกเตอร์หนึ่งหน่วยในพิกัดฉาก ซึ่งจะเหมือนกับในหัวข้อการแปลงเวกเตอร์ในพิกัดแบบทรงกระบอก [1.8.1](ch1-08-01TH.md#มุมระหว่างเวกเตอร์หนึ่งหน่วย)  เราได้ว่า    
```math
\beta_{\phi,x} = \frac {\pi}{2}+\phi \newline 
\beta_{\phi,y} = \phi \newline 
\beta_{\phi,z} = \frac{\pi}{2} \newline 
```
จากรูป $`(a)`$ เรายังทราบมุมระหว่าง  $`\mathbf a_r`$ กับ  $`\mathbf a_z`$ คือ $`\beta_{r,z} = \theta`$ และ มุมระหว่าง  $`\mathbf a_\theta`$ กับ  $`\mathbf a_z`$ คือ $`\beta_{\theta,z} = \frac {\pi}{2}+\theta `$ และขนาดของเวกเตอร์หนึ่งหน่วย $`\mathbf a`$ เท่ากับหนึ่ง ดังนั้นเราจะได้ว่า 
```math
k_{\phi,x} = \cos \left( \frac{\pi}{2}+\phi   \right) = -\sin \phi \newline 
k_{\phi,y} = \cos \phi \newline  
k_{\phi,z} = \cos \frac{\pi}{2} = 0\newline  
k_{r,z} = \cos \theta \newline  
k_{\theta,z} = \cos \left( \frac{\pi}{2}+\theta   \right) = -\sin \theta \newline  
```  
ซึ่งเราจะได้ว่า $`\mathbf a_\phi = -\sin \phi \mathbf a_x + \cos \phi \mathbf a_y`$ เหมือนกับในหัวข้อการแปลงเวกเตอร์ในพิกัดแบบทรงกระบอก [1.8.1](ch1-08-01TH.md#มุมระหว่างเวกเตอร์หนึ่งหน่วย) แต่เราไม่ได้นิยามตัวแปรสำหรับแทนมุมระหว่าง  $`\mathbf a_r`$ กับ เวกเตอร์หนึ่งหน่วยอื่นๆในพิกัดฉาก และมุมระหว่าง  $`\mathbf a_\theta`$ กับ เวกเตอร์หนึ่งหน่วยอื่นๆในพิกัดฉาก เราต้องหาค่าของ $`k_{r,x}, k_{r,y}, k_{\theta,x}, k_{\theta,y} `$ โดยใช้ตัวแปรมุม $`\theta`$ กับ $`\phi`$ เท่านั้น เราจะเขียน $`\mathbf a_r`$ กับ $`\mathbf a_\theta`$ ในรูปของ $`\mathbf a_\rho`$ กับ $`\mathbf a_z`$ ก่อนโดยมองเวกเตอร์บนระนาบ $`\rho z`$ ในรูป $`(a)`$ เราะจะได้ว่า 

```math
\mathbf a_r = k_{r,\rho} \mathbf a_\rho + k_{r,z} \mathbf a_z\newline  
\mathbf a_\theta = k_{\theta,\rho} \mathbf a_\rho + k_{\theta,z} \mathbf a_z \newline  
```  
ในรูป $`(a)`$ บนระนาบ $`\rho z`$ มุมระหว่าง $`\mathbf a_r`$ กับ $`\mathbf a_\rho`$ คือ $`\frac{\pi}{2}-\theta`$ และ มุมระหว่าง $`\mathbf a_\theta`$ กับ $`\mathbf a_\rho`$ คือ $`\theta`$ ซึ่ง $`k_{i,j}`$ คือ
```math
k_{r,\rho} =\mathbf a_r \cdot \mathbf a_\rho =  \cos \left( \frac{\pi}{2}-\theta   \right) = \sin \theta \newline  
k_{\theta,\rho} =\mathbf a_\theta \cdot \mathbf a_\rho  = \cos \theta \newline  
```  
แทนค่า $`k_{i,j}`$ ใน  $`\mathbf a_r`$ และ  $`\mathbf a_\theta`$ เราจะได้  
```math
\mathbf a_r =  \sin \theta \mathbf a_\rho  +  \cos \theta \mathbf a_z\newline  
\mathbf a_\theta = \cos \theta \mathbf a_\rho -\sin \theta \mathbf a_z \newline  
```  
แทน $`\mathbf a_\rho = \left(\cos \phi \mathbf a_x + \sin \phi  \mathbf a_y\right)`$ จากหัวข้อ [1.8.1](ch1-08-01TH.md#มุมระหว่างเวกเตอร์หนึ่งหน่วย) เราได้ว่า
```math
\mathbf a_r =  \sin \theta \left(\cos \phi \mathbf a_x + \sin \phi  \mathbf a_y\right)  +  \cos \theta \mathbf a_z\newline  
\mathbf a_\theta = \cos \theta \left(\cos \phi \mathbf a_x + \sin \phi  \mathbf a_y\right) -\sin \theta \mathbf a_z \newline  
```  
ดังนั้นเราจะได้  
```math
\mathbf a_r =  \sin \theta \cos \phi \mathbf a_x + \sin \theta \sin \phi  \mathbf a_y  +  \cos \theta \mathbf a_z\newline  
\mathbf a_\theta = \cos \theta \cos \phi \mathbf a_x + \cos \theta \sin \phi  \mathbf a_y -\sin \theta \mathbf a_z \newline  
\mathbf a_\phi = -\sin \phi \mathbf a_x + \cos \phi \mathbf a_y \newline   
```  
ถ้าเรามีแรง $` \mathbf F = F_x \mathbf a_x + F_y \mathbf a_y + F_z \mathbf a_z = F_r \mathbf a_r+ F_\theta \mathbf a_\theta+ F_z \mathbf a_\phi `$ จะได้ว่า  
```math
F_x \mathbf a_x + F_y \mathbf a_y + F_z \mathbf a_z = F_r \mathbf a_r+ F_\theta \mathbf a_\theta+ F_\phi \mathbf a_\phi \newline

F_x \mathbf a_x + F_y \mathbf a_y + F_z \mathbf a_z = F_r \left(\sin \theta \cos \phi \mathbf a_x + \sin \theta \sin \phi  \mathbf a_y  +  \cos \theta \mathbf a_z\right)+ F_\theta \left( \cos \theta \cos \phi \mathbf a_x + \cos \theta \sin \phi  \mathbf a_y -\sin \theta \mathbf a_z \right)+ F_\phi \left( -\sin \phi \mathbf a_x + \cos \phi \mathbf a_y\right) \newline

F_x \mathbf a_x + F_y \mathbf a_y + F_z \mathbf a_z = 
\left(F_r \sin \theta \cos \phi +F_\theta  \cos \theta \cos \phi -F_\phi \sin \phi \right) \mathbf a_x + 
\left(F_r \sin \theta \sin \phi +F_\theta  \cos \theta \sin \phi +F_\phi \cos \phi \right) \mathbf a_y +
\left(F_r \cos \theta  -F_\theta  \sin \theta \right) \mathbf a_z \newline


```
หรือเขียนอยู่ในรูปการคูณแบบเมตริกได้ดังนี้  

```math
\mathbf y = \mathbf A \mathbf x \newline
\begin{bmatrix} F_x\\F_y\\F_z \end{bmatrix} =  
\begin{bmatrix} \sin\theta \cos \phi&\cos \theta \cos \phi&-\sin \phi \\
\sin \theta \sin \phi & \cos \theta \sin \phi & \cos \phi\\ 
\cos \theta &-\sin \theta & 0\end{bmatrix}  
\begin{bmatrix} F_r\\F_\theta\\F_\phi \end{bmatrix}
```  
จะเห็นว่า $`\mathbf A^T \mathbf A = \mathbf I`$ หรือ $`\mathbf A^T = \mathbf A^{-1}`$ ดังนั้นในทางกลับกันเราจะได้   
```math
\mathbf x = \mathbf A^{-1} \mathbf y \newline
\begin{bmatrix} F_r\\F_\theta\\F_\phi \end{bmatrix} =  
\begin{bmatrix} \sin\theta \cos \phi&\cos \theta \cos \phi&-\sin \phi \\
\sin \theta \sin \phi & \cos \theta \sin \phi & \cos \phi\\ 
\cos \theta &-\sin \theta & 0\end{bmatrix}^{-1}  
\begin{bmatrix} F_x\\F_y\\F_z \end{bmatrix} \newline
 \newline
\begin{bmatrix} F_r\\F_\theta\\F_\phi \end{bmatrix} =  
\begin{bmatrix} \sin\theta \cos \phi&\sin \theta \sin \phi&\cos \theta \\
\cos \theta \cos \phi & \cos \theta \sin \phi & -\sin \theta\\ 
-\sin \phi & \cos \phi& 0\end{bmatrix}  
\begin{bmatrix} F_x\\F_y\\F_z \end{bmatrix} \newline
```  


<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/sphere_cartesian.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(a) แสดงทิศทางของเวกเตอร์หนึ่งหน่วยที่จุด $`P`$ และจุด $`Q`$ (ดัดแปลงจาก [Hayt 2011](#Hayt))


อ้างอิง  
- <a name="Hayt"></a> Hayt, W. H., & Buck, J. A. (2011). Engineering Electromagnetics (8th ed.). McGraw-Hill Professional.

 

|[ก่อนหน้า](ch1-08-01TH.md)| [ต่อไป](ch1-08-03TH.md) |[ต่อไป บทที่ 2](ch2TH.md) |
| ---------- | ---------- | ---------- |    

[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  



